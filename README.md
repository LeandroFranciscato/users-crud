
# Users

Users is a Typescript application that manage a User CRUD

# Dependencies

You shoud have installed [docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/) in order do set it up.


## Installation

##### Step-by-step
- Clone de project: `git clone https://gitlab.com/LeandroFranciscato/users-crud.git`
- Enter the folder: `cd users-crud`
- Grant execution permission on `setup.sh` file: `chmod +x ./ci/setup.sh`
- Run it: `./ci/setup.sh`

## Usage

You should be able to access the API of the project on: `localhost:3100`
There, you could use our insomnia file (within the insomnia directory) to consume the API.

You could also choose another client just hiting the following routes:

| Method  | Route  | Parameters  | Description  |   
|---|---|---|---|
| GET  | /developers  |   | Get all users   |
| GET  | /developers/:id  |   |  Get a specific user by id  |  
|  GET | /developers/filter  | ?page=1&perPage=5&term=search  | Get a list of users by term, paginating it  | 
|  POST | /developers  | {id, name, sex, age, hobby, birthdate}  | Create a new User  | 
|  PUT | /developers/:id  | {id, name, sex, age, hobby, birthdate}  | Update a User  | 
|  DELETE | /developers/:id  |   | Delete a User  | 

## Developer mode

Locally, for development and tests porpouse, you could use a `.env` (in the root folder) for the project variables. <br>
And, `yarn start:dev` for the project to run using [Nodemon](https://www.npmjs.com/package/nodemon).

Something like that

#.env
```env
NODE_ENV=local
SERVER_HOST=localhost
SERVER_PORT=3100
DATABASE_URL=postgres://postgres:postgres@localhost:5432/users
DATABASE_URL_READ_REPLICA=postgres://postgres:postgres@localhost:5432/users
ORM_ROOT_DIR=src
ORM_LOGGING=false
ORM_SYNCHRONIZE=false
```
#running it
```bash
yarn start:dev
```

# Database Maintenance
We're using [Typeorm](https://typeorm.io/#/) as a persistence framework.
Said that, we use migrations to manipulate our database, so... 

In order to generate a new migration, you run:
```sh
yarn migration:generate new-column-on-user-table
```

In order to modify your development/local database, you run:
```sh
yarn migration:run
```

Ps: Generated migrations are stored on `.src/domain/migration` folder, and they run automatically when `./ci/setup.sh` ran.

# Tests
We use [Jest](https://jestjs.io/) and [Supertest](https://github.com/visionmedia/supertest) for unit tests pourpouse.
In order to verify the project tests, it's cover and debug it, you could run: `yarn test`

## License
[MIT](https://choosealicense.com/licenses/mit/)

const SnakeNamingStrategy = require("typeorm-naming-strategies").SnakeNamingStrategy

const rootDir = process.env.ORM_ROOT_DIR;

module.exports = [
  {
    name: "default",
    type: "postgres",
    url: process.env.DATABASE_URL,
    synchronize: false,
    logging: process.env.ORM_LOGGING,
    entities: [rootDir + "/domain/entity/*.{js,ts}"],
    migrations: [rootDir + "/domain/migration/*.{js,ts}"],
    cli: {
      entitiesDir: "src/domain/entity",
      migrationsDir: "src/domain/migration",
    },
    namingStrategy: new SnakeNamingStrategy()
  },
  {
    name: "second-connection",
    type: "postgres",
    url: process.env.DATABASE_URL_READ_REPLICA,
    synchronize: false,
    logging: process.env.ORM_LOGGING,
    entities: [rootDir + "/domain/entity/*.{js,ts}"],
    migrations: [rootDir + "/domain/migration/*.{js,ts}"],
    cli: {
      entitiesDir: "src/domain/entity",
      migrationsDir: "src/domain/migration",
    },
    namingStrategy: new SnakeNamingStrategy()
  },
];

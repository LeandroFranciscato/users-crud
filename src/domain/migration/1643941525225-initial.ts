import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1643941525225 implements MigrationInterface {
    name = 'initial1643941525225'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "Users" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "sex" character varying NOT NULL, "age" integer NOT NULL, "hobby" character varying NOT NULL, "birthdate" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_16d4f7d636df336db11d87413e3" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "Users"`);
    }

}

import {
  init as InitUserRepository,
  User,
  UserRepository,
} from "../repository/User";

import { init as InitLogger } from "../app/common/logger";
import winston from "winston";
import { UserPartial } from "../domain/entity/User";

export class UserUseCase {
  private userRepository: UserRepository;
  private log: winston.Logger;

  constructor(userRepository, logger) {
    this.userRepository = userRepository;
    this.log = logger;
  }

  public getAll = async (): Promise<User[]> => {
    return this.userRepository.getAll();
  };

  public getByTerm = async (query: {
    page: number;
    perPage: number;
    term: string;
  }): Promise<[User[], number]> => {
    return this.userRepository.getByTerm(query);
  };

  public getById = async (id: number): Promise<User> => {
    return this.userRepository.getById(id);
  };

  public create = async ({
    user,
  }: {
    user: User;
  }): Promise<{ user: UserPartial; error: string }> => {
    try {
      const newUser = await this.userRepository.save(user);
      return { user: newUser, error: "" };
    } catch (e) {
      return { user: {}, error: e };
    }
  };

  public update = async ({
    user,
  }: {
    user: User;
  }): Promise<{ user: UserPartial; error: string }> => {
    try {
      const currentUser = await this.getById(user.id);

      if (!currentUser) {
        return {
          user: {},
          error: "Couldn't find the updating user, check it up!",
        };
      }

      const newUser = await this.userRepository.save(user);
      return { user: newUser, error: "" };
    } catch (e) {
      return { user: {}, error: e };
    }
  };

  public delete = async (id: number): Promise<{ error: string }> => {
    try {
      this.userRepository.delete(id);
    } catch (e) {
      return { error: e };
    }
  };
}

export const init = () => {
  const userRepository = InitUserRepository();
  const logger = InitLogger();
  return new UserUseCase(userRepository, logger);
};

export default init;

import { userRepository, log } from "../mockDependencies";
import { UserUseCase } from "../../User";
import { UserPartial, User } from "../../../domain/entity/User";

it("Create User ", async () => {
  const userUsecase = new UserUseCase(userRepository, log);

  userRepository.delete.mockResolvedValue(undefined);
  expect(await userUsecase.delete(1)).toEqual(undefined);

  expect(userRepository.delete).toHaveBeenCalledWith(1);
});

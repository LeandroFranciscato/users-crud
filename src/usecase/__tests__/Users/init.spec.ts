import { init, UserUseCase } from "../../User";

import { init as InitUserRepository } from "../../../repository/User";

jest.mock("../../../repository/User");

it("inits the User use case correctly", () => {
  expect(init()).toBeInstanceOf(UserUseCase);
  expect(InitUserRepository).toHaveBeenCalled();
});

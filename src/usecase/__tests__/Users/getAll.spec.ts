import { userRepository, log } from "../mockDependencies";
import { UserUseCase } from "../../User";
import { UserPartial, User } from "../../../domain/entity/User";

it("should get All Users ", async () => {
  const userUsecase = new UserUseCase(userRepository, log);

  const results: UserPartial[] = [{ id: 1 }, { id: 2 }];

  userRepository.getAll.mockResolvedValue(results);
  expect(await userUsecase.getAll()).toEqual(results);

  expect(userRepository.getAll).toHaveBeenCalledWith();
});

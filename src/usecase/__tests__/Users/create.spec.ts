import { userRepository, log } from "../mockDependencies";
import { UserUseCase } from "../../User";
import { UserPartial, User } from "../../../domain/entity/User";

it("Create User ", async () => {
  const userUsecase = new UserUseCase(userRepository, log);

  const results: UserPartial = { id: 1 };

  userRepository.save.mockResolvedValue(results);
  expect(await userUsecase.create({ user: { id: 1 } as User })).toEqual({
    user: results,
    error: "",
  });

  expect(userRepository.save).toHaveBeenCalledWith({ id: 1 } as User);
});

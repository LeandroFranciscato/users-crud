import { userRepository, log } from "../mockDependencies";
import { UserUseCase } from "../../User";
import { UserPartial, User } from "../../../domain/entity/User";

it("should get User By Id ", async () => {
  const userUsecase = new UserUseCase(userRepository, log);

  const results: UserPartial = { id: 1 };

  userRepository.getById.mockResolvedValue(results);
  expect(await userUsecase.getById(1)).toEqual(results);

  expect(userRepository.getById).toHaveBeenCalledWith(1);
});

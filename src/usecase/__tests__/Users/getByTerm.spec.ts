import { userRepository, log } from "../mockDependencies";
import { UserUseCase } from "../../User";
import { UserPartial, User } from "../../../domain/entity/User";

it("should get Users by term with pagination ", async () => {
  const userUsecase = new UserUseCase(userRepository, log);

  const results: [UserPartial[], number] = [[{ id: 1 }, { id: 2 }], 1];

  userRepository.getByTerm.mockResolvedValue(results);
  expect(
    await userUsecase.getByTerm({ page: 1, perPage: 1, term: "le" })
  ).toEqual(results);

  expect(userRepository.getByTerm).toHaveBeenCalledWith({
    page: 1,
    perPage: 1,
    term: "le",
  });
});

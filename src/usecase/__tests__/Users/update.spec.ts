import { userRepository, log } from "../mockDependencies";
import { UserUseCase } from "../../User";
import { UserPartial, User } from "../../../domain/entity/User";

it("Update User ", async () => {
  const userUsecase = new UserUseCase(userRepository, log);

  const results: UserPartial = { id: 1 };

  userRepository.getById.mockResolvedValue({} as User);
  userRepository.save.mockResolvedValue(results);
  expect(await userUsecase.update({ user: { id: 1 } as User })).toEqual({
    user: results,
    error: "",
  });

  expect(userRepository.save).toHaveBeenCalledWith({ id: 1 } as User);
  expect(userRepository.getById).toHaveBeenCalledWith(1);
});

it("Fail updating non existing User ", async () => {
  const userUsecase = new UserUseCase(userRepository, log);

  const results: UserPartial = {};

  userRepository.getById.mockResolvedValue(undefined);
  userRepository.save.mockResolvedValue(results);
  expect(await userUsecase.update({ user: { id: 1 } as User })).toEqual({
    user: results,
    error: "Couldn't find the updating user, check it up!",
  });

  expect(userRepository.save).toBeCalledTimes(0);
  expect(userRepository.getById).toHaveBeenCalledWith(1);
});

export const log = {
  info: jest.fn(),
  error: jest.fn(),
};

export const userRepository = {
  getAll: jest.fn(),
  getByTerm: jest.fn(),
  getById: jest.fn(),
  delete: jest.fn(),
  save: jest.fn(),
};

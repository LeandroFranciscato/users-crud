import * as morgan from "morgan";
import * as promBundle from "express-prom-bundle";
import * as compression from "compression";
import * as bodyParser from "body-parser";

import logger from "../../app/common/logger";
import tracer from "../../app/common/tracer";

import { init as InitErrorMiddleware } from "./middleware/Error";

import SignatureHandler from "./handler/User";

const defineMiddelwares = async (router, express) => {
  router.use(tracer.middlewareForExpress());

  router.use(
    promBundle({
      includeMethod: true,
      includePath: true,
    })
  );

  if (
    process.env.NODE_ENV === "local" ||
    process.env.NODE_ENV === "development"
  ) {
    router.use(morgan("dev"));
  } else {
    router.use(morgan("combined", logger.morganOptions));
  }

  router.use(compression());
  router.use(bodyParser.json({ limit: "50mb" }));
  router.use(
    bodyParser.urlencoded({
      limit: "50mb",
      extended: true,
      parameterLimit: 50000,
    })
  );
  router.get(["/health", "/public/health"], function mainHandler(req, res) {
    res.status(200).json("ok");
  });
};

const defineHandlers = () => {
  SignatureHandler();
};

const init = async (router, express) => {
  // Define middlewares & handlers
  await defineMiddelwares(router, express);
  defineHandlers();

  // Define error
  InitErrorMiddleware();
};

export default { init };

export const userUseCase = {
  getAll: jest.fn(),
  getByTerm: jest.fn(),
  getById: jest.fn(),
  delete: jest.fn(),
  create: jest.fn(),
  update: jest.fn(),
};

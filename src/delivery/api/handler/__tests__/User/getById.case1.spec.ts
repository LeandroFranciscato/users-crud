import * as request from "supertest";
import * as express from "express";
import { UserHandler } from "../../User";

import { User, UserPartial } from "../../../../../domain/entity/User";

const router = express();

import { userUseCase } from "../mockDependencies";

/* tslint:disable-next-line */
it("Get a status 400 because of invalid id", async (done) => {
  /* tslint:disable-next-line */
  new UserHandler(router, userUseCase);

  userUseCase.getById.mockReturnValueOnce({});

  request(router)
    .get("/developers/a")
    .set("Accept", "application/json")
    .expect("Content-Type", /json/)
    .expect(400, (err, res) => {
      expect(err).toBeNull();

      expect(userUseCase.getById).toHaveBeenCalledTimes(0);
      expect(JSON.stringify(res.body)).toEqual(
        JSON.stringify({ user: {}, error: "Invalid id" })
      );

      done();
    });
});

import * as request from "supertest";
import * as express from "express";
import { UserHandler } from "../../User";

import { User, UserPartial } from "../../../../../domain/entity/User";

const router = express();

import { userUseCase } from "../mockDependencies";

/* tslint:disable-next-line */
it("Get All users and return statuscode 200", async (done) => {
  /* tslint:disable-next-line */
  new UserHandler(router, userUseCase);
  const usersExpected: UserPartial[] = [{ id: 1 }, { id: 10 }];

  userUseCase.getAll.mockReturnValueOnce(usersExpected);

  request(router)
    .get("/developers")
    .set("Accept", "application/json")
    .expect("Content-Type", /json/)
    .expect(200, (err, res) => {
      expect(err).toBeNull();

      expect(userUseCase.getAll).toHaveBeenCalledWith();
      expect(JSON.stringify(res.body)).toEqual(
        JSON.stringify([{ id: 1 }, { id: 10 }])
      );

      done();
    });
});

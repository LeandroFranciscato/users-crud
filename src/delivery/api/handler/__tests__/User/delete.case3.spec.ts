import * as request from "supertest";
import * as express from "express";
import { UserHandler } from "../../User";

import { User, UserPartial } from "../../../../../domain/entity/User";

const router = express();

import { userUseCase } from "../mockDependencies";

/* tslint:disable-next-line */
it("any error on user use case", async (done) => {
  /* tslint:disable-next-line */
  new UserHandler(router, userUseCase);

  userUseCase.delete.mockReturnValueOnce("");

  request(router)
    .delete("/developers/1")
    .set("Accept", "application/json")
    .expect("Content-Type", /json/)
    .expect(200, (err, res) => {
      expect(err).toBeNull();

      expect(userUseCase.delete).toHaveBeenCalledWith(1);
      expect(JSON.stringify(res.body)).toEqual(JSON.stringify(""));

      done();
    });
});

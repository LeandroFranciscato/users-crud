import * as request from "supertest";
import * as express from "express";
import { UserHandler } from "../../User";

import { User, UserPartial } from "../../../../../domain/entity/User";

const router = express();

var bodyParser = require("body-parser");
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

import { userUseCase } from "../mockDependencies";

/* tslint:disable-next-line */
it("Invalid id", async (done) => {
  /* tslint:disable-next-line */
  new UserHandler(router, userUseCase);

  userUseCase.update.mockReturnValueOnce(undefined);

  request(router)
    .put("/developers/a")
    .send({
      name: "asd",
      sex: "M",
      age: 34,
      hobby: "Programming",
      birthdate: "1987-09-11 00:00:00.000000+00",
      id: 1,
    })
    .set("Accept", "application/json")
    .expect("Content-Type", /json/)
    .expect(400, (err, res) => {
      expect(err).toBeNull();

      expect(userUseCase.update).toHaveBeenCalledTimes(0);
      expect(JSON.stringify(res.body)).toEqual(
        JSON.stringify({
          user: {},
          error: "Invalid id",
        })
      );

      done();
    });
});

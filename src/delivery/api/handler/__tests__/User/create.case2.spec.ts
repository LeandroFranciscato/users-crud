import * as request from "supertest";
import * as express from "express";
import { UserHandler } from "../../User";

import { User, UserPartial } from "../../../../../domain/entity/User";

const router = express();

var bodyParser = require("body-parser");
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

import { userUseCase } from "../mockDependencies";

/* tslint:disable-next-line */
it("Any error on create usecase", async (done) => {
  /* tslint:disable-next-line */
  new UserHandler(router, userUseCase);

  userUseCase.create.mockReturnValueOnce({
    user: {},
    error: "any error",
  });

  request(router)
    .post("/developers")
    .send({
      name: "asd",
      sex: "M",
      age: 34,
      hobby: "Programming",
      birthdate: "1987-09-11 00:00:00.000000+00",
    })
    .set("Accept", "application/json")
    .expect("Content-Type", /json/)
    .expect(400, (err, res) => {
      expect(err).toBeNull();

      expect(userUseCase.create).toHaveBeenCalledWith({
        user: {
          name: "asd",
          sex: "M",
          age: 34,
          hobby: "Programming",
          birthdate: "1987-09-11 00:00:00.000000+00",
        },
      });
      expect(JSON.stringify(res.body)).toEqual(
        JSON.stringify({
          user: {},
          error: "any error",
        })
      );

      done();
    });
});

import * as request from "supertest";
import * as express from "express";
import { UserHandler } from "../../User";

import { User, UserPartial } from "../../../../../domain/entity/User";

const router = express();

import { userUseCase } from "../mockDependencies";

/* tslint:disable-next-line */
it("Get a User", async (done) => {
  /* tslint:disable-next-line */
  new UserHandler(router, userUseCase);
  const userExpected: UserPartial = { id: 1 };

  userUseCase.getById.mockReturnValueOnce(userExpected);

  request(router)
    .get("/developers/1")
    .set("Accept", "application/json")
    .expect("Content-Type", /json/)
    .expect(200, (err, res) => {
      expect(err).toBeNull();

      expect(userUseCase.getById).toHaveBeenCalledWith(1);
      expect(JSON.stringify(res.body)).toEqual(JSON.stringify(userExpected));

      done();
    });
});

import * as request from "supertest";
import * as express from "express";
import { UserHandler } from "../../User";

import { User, UserPartial } from "../../../../../domain/entity/User";

const router = express();

import { userUseCase } from "../mockDependencies";

/* tslint:disable-next-line */
it("Invalid page parameter", async (done) => {
  /* tslint:disable-next-line */
  new UserHandler(router, userUseCase);

  const response = {
    data: {},
    total: 0,
    error: "",
  };

  userUseCase.getByTerm.mockReturnValueOnce(undefined);

  request(router)
    .get("/developers/filter?page=a&perPage=2&term=a")
    .set("Accept", "application/json")
    .expect("Content-Type", /json/)
    .expect(400, (err, res) => {
      expect(err).toBeNull();

      expect(userUseCase.getByTerm).toHaveBeenCalledTimes(0);

      response.error = "Invalid page parameter";
      expect(JSON.stringify(res.body)).toEqual(JSON.stringify(response));

      done();
    });
});

import * as request from "supertest";
import * as express from "express";
import { UserHandler } from "../../User";

import { User, UserPartial } from "../../../../../domain/entity/User";

const router = express();

import { userUseCase } from "../mockDependencies";

/* tslint:disable-next-line */
it("get Users by term", async (done) => {
  /* tslint:disable-next-line */
  new UserHandler(router, userUseCase);

  const response = {
    data: [{ id: 1 }, { id: 2 }] as User[],
    total: 10,
    error: "",
  };

  userUseCase.getByTerm.mockReturnValueOnce([[{ id: 1 }, { id: 2 }], 10] as [
    User[],
    number
  ]);

  request(router)
    .get("/developers/filter?page=1&perPage=2&term=a")
    .set("Accept", "application/json")
    .expect("Content-Type", /json/)
    .expect(200, (err, res) => {
      expect(err).toBeNull();

      expect(userUseCase.getByTerm).toHaveBeenCalledWith({
        page: 1,
        perPage: 2,
        term: "a",
      });

      expect(JSON.stringify(res.body)).toEqual(JSON.stringify(response));

      done();
    });
});

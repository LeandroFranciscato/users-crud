import {
  init as InitRouter,
  Request,
  Response,
  NextFunction,
} from "../../../driver/http/express";
import { User } from "../../../repository/User";

import { UserUseCase, init as InitUserUseCase } from "../../../usecase/User";

export class UserHandler {
  private userUseCase: UserUseCase;

  constructor(router, userUseCase) {
    this.userUseCase = userUseCase;
    router.get("/developers", this.getAll);
    router.get("/developers/filter", this.getByTerm);
    router.get("/developers/:id", this.getById);
    router.post("/developers", this.create);
    router.put("/developers/:id", this.update);
    router.delete("/developers/:id", this.delete);
  }

  public getAll = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await this.userUseCase.getAll();
      res.json(data);
    } catch (error) {
      res.status(500).json({ users: [{}], error: error });
    }
  };

  public getByTerm = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const response = {
      data: {},
      total: 0,
      error: "",
    };

    try {
      const page = Number(req.query.page);

      if (!page) {
        response.error = "Invalid page parameter";
        res.status(400).json(response);
        return;
      }

      const perPage = Number(req.query.perPage);

      if (!perPage) {
        response.error = "Invalid perPage parameter";
        res.status(400).json(response);
        return;
      }

      const term = String(req.query.term);

      const [data, total] = await this.userUseCase.getByTerm({
        page,
        perPage,
        term,
      });

      response.data = data;
      response.total = total;
      res.json(response);
    } catch (error) {
      response.error = error;
      res.status(500).json(response);
    }
  };

  public getById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id = Number(req.params.id);

      if (!id) {
        res.status(400).json({ user: {}, error: "Invalid id" });
        return;
      }

      const data = await this.userUseCase.getById(id);
      res.json(data);
    } catch (error) {
      res.status(500).json({ user: {}, error: error });
    }
  };

  public create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const userBody: User = req.body;
      if (userBody.id) {
        res.status(400).json({
          user: {},
          error: "You should not inform the 'id' field",
        });
        return;
      }

      const data = await this.userUseCase.create({
        user: userBody,
      });

      if (data.error) {
        res.status(400).json(data);
        return;
      }

      res.json(data);
    } catch (error) {
      res.status(500).json({ user: {}, error: error });
    }
  };

  public update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id = Number(req.params.id);

      if (!id) {
        res.status(400).json({ user: {}, error: "Invalid id" });
        return;
      }

      const updatedUser: User = req.body;
      if (updatedUser.id != id) {
        res.status(400).json({
          user: {},
          error:
            "The user updating information didn't match with the id that you've send!",
        });
        return;
      }

      const data = await this.userUseCase.update({
        user: req.body,
      });

      if (data.error) {
        res.status(400).json(data);
        return;
      }

      res.json(data);
    } catch (error) {
      res.status(500).json({
        user: {},
        error: error,
      });
    }
  };

  public delete = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id = Number(req.params.id);

      if (!id) {
        res.status(400).json({ error: "Invalid id" });
        return;
      }

      const error = await this.userUseCase.delete(id);

      if (error) {
        res.status(400).json({ error: error });
        return;
      }

      res.json(error);
    } catch (error) {
      res.status(500).json({ error: error });
    }
  };
}

export const init = () => {
  const router = InitRouter();
  const userUseCase = InitUserUseCase();

  return new UserHandler(router, userUseCase);
};

export default init;

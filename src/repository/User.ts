import { User } from "../domain/entity/User";
import {
  init as InitDB,
  Repository,
  ConnectionWrite,
  ConnectionRead,
} from "../driver/database/postgres";

export { User };

export class UserRepository {
  private dbWrite: Repository<User>;
  private dbRead: Repository<User>;

  constructor(dbWrite: ConnectionWrite, dbRead: ConnectionRead) {
    this.dbWrite = dbWrite.getRepository(User);
    this.dbRead = dbRead.getRepository(User);
  }

  public getAll = async (): Promise<User[]> => {
    return this.dbRead.createQueryBuilder("user").getMany();
  };

  public getByTerm = async (query: {
    page: number;
    perPage: number;
    term: string;
  }): Promise<[User[], number]> => {
    const { page, perPage, term } = query;
    const queryBuilder = this.dbRead
      .createQueryBuilder("user")
      .where("name like :term", { term: `%${term}%` })
      .orWhere("cast(sex as text) like :term", { term: `%${term}%` })
      .orWhere("hobby like :term", { term: `%${term}%` })
      .orWhere("cast(id as text) like :term", { term: `%${term}%` })
      .orWhere("cast(age as text) like :term", { term: `%${term}%` })
      .orWhere("cast(birthdate as text) like :term", { term: `%${term}%` });

    const skip = perPage * page - perPage;

    return queryBuilder.skip(skip).take(perPage).getManyAndCount();
  };

  public getById = async (id: number): Promise<User> => {
    return this.dbRead
      .createQueryBuilder("user")
      .where("id = :id", { id })
      .getOne();
  };

  public save = async (user: User): Promise<User> => {
    return this.dbWrite.save(user);
  };

  public delete = async (id: number): Promise<void> => {
    this.dbWrite
      .createQueryBuilder()
      .delete()
      .from(User)
      .where("id = :id", { id: id })
      .execute();
  };
}

export const init = () => {
  const dbWrite = InitDB();
  const dbRead = InitDB().read;

  return new UserRepository(dbWrite, dbRead);
};

export default init;

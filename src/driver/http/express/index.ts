import * as http from "http";
import * as express from "express";

export { Request, Response, NextFunction } from "express";

let router: express.Application;
let server: http.Server;

/**
 * Create a new app express and http server
 */
export const create = () => {
  if (router) {
    throw new Error("http server already created");
  }

  router = express();
  server = http.createServer(router);

  return { router, server, express };
};

/**
 * Return a existing app express
 * @returns
 */
export const init = () => {
  if (!router) {
    throw new Error("http server not created");
  }

  return router;
};

/**
 * Return a existing app express and http server
 * @returns
 */
export const initWithServer = () => {
  if (!router) {
    throw new Error("http server not created");
  }

  return { router, server };
};

export default { create, init, initWithServer };

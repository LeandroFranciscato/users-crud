import { Connection } from "typeorm";

export { Repository, DeleteResult, TreeRepository } from "typeorm";

export type ConnectionWrite = Connection & { read: Connection };
export type ConnectionRead = Connection;

let connectionWrite: ConnectionWrite;
let connectionRead: ConnectionRead;

/**
 * Define connection with Database
 * @param cnn
 */
export const set = ([cnn, read]) => {
  if (connectionWrite) {
    throw new Error("already connected");
  }

  connectionWrite = cnn;
  connectionRead = read;
};

/**
 * Return existing connection for Datavase
 * @returns
 */
export const init = () => {
  if (!connectionWrite) {
    throw new Error("unable connection");
  }

  connectionWrite.read = connectionRead;
  return connectionWrite;
};

export default { init, set };

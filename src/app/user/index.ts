import * as dotenv from "dotenv";
dotenv.config({ path: process.env.DOTENV_PATH || undefined });

import {
  ConnectionOptions,
  createConnections,
  getConnectionOptions,
} from "typeorm";
import "reflect-metadata";

import { init as InitLogger, Logger } from "../common/logger";
import signal from "../common/signal";

import expressInit from "../../driver/http/express";
import postgres from "../../driver/database/postgres";

import api from "../../delivery/api";
import { Profiler } from "winston";

let log: Logger;

let databaseTimer: Profiler;
let holisticTimer: Profiler;

/**
 * Main application execution
 * @param connection Database connection
 * @async
 */
/* tslint:disable-next-line */
const main = async (connection) => {
  // Database
  postgres.set(connection);
  databaseTimer.done({ message: "Database connection stablished" });

  try {
    // Http Server
    const { router, server, express } = expressInit.create();

    const apiInitTimer = log.startTimer();
    // Initialize API Rest
    await api.init(router, express);

    apiInitTimer.done({ message: "API endpoints configured" });

    // Apply callbacks
    process.on("SIGTERM", signal.shutdown);
    process.on("SIGINT", signal.shutdown);

    // Listen
    server.listen(process.env.SERVER_PORT, (err?) => {
      if (err) {
        throw err;
      }

      log.info(
        `Users is running at http://${process.env.SERVER_HOST}:${
          process.env.SERVER_PORT
        } in ${router.get("env")} mode ${process.env.NODE_ENV}`
      );
      holisticTimer.done({ message: "Total time" });
    });
  } catch (e) {
    log.error(e.message);
    process.exit(1);
  }
};

const removeMigrationsProperty = (connectionOptions: ConnectionOptions) => {
  const { migrations, ...remainingConnectionOptions } = connectionOptions;

  return remainingConnectionOptions;
};

/**
 * Start application
 * @async
 */
/* tslint:disable-next-line */
const run = async () => {
  log = InitLogger();

  databaseTimer = log.startTimer();
  holisticTimer = log.startTimer();

  const writeDbConnectionOptions = await getConnectionOptions("default");
  const readReplicaConnectionOptions = await getConnectionOptions(
    "second-connection"
  );

  const connectionsOptions = [
    removeMigrationsProperty(writeDbConnectionOptions),
    removeMigrationsProperty(readReplicaConnectionOptions),
  ];

  // Connect Database
  createConnections(connectionsOptions)
    .then(main)
    .catch((e) => {
      log.error(e.message);
    });
};

// Start
run();

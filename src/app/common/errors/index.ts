interface IError {
  code: string;
  status?: number;
  message?: string;
}

const errors: IError[] = [
  {
    code: "1001",
    status: 500,
  },
  {
    code: "23503",
    status: 400,
    message: "It is not possible to delete with associated content",
  },
  {
    code: "23505",
    status: 400,
    message: "Content already registered",
  },
  {
    code: "22P02",
    status: 400,
    message: "Invalid input syntax for type integer: 'NaN'",
  },
  { code: "1002" },

  {
    code: "401",
    status: 401,
    message: "401 Unauthorized",
  },
  {
    code: "1003",
    status: 400,
    message: "Cloud not found",
  },

  {
    code: "1020",
    status: 400,
    message: "JSON invalid",
  },
  {
    code: "1020",
    status: 400,
    message: "JSON invalid",
  },
  {
    code: "1008",
    status: 404,
    message: "incident invalid",
  },
  {
    code: "1004",
    status: 500,
    message: "Slack code verification failed",
  },
  {
    code: "400",
    status: 400,
    message: "Bad request",
  },
];

export default errors;

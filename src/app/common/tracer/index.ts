import { v4 as uuidv4 } from "uuid";
import { AsyncLocalStorage } from "async_hooks";

const asyncStorage = new AsyncLocalStorage();

export const headerName = "x-ew-request-id";

/**
 * Generate a randon request-id
 * @returns
 */
export const createRequestId = (): string => uuidv4();

/**
 * Get request-id from context (if exists)
 */
export const getRequestId = () => asyncStorage.getStore();

/**
 * Inject request-id headers for axios fetch
 * @param header
 * @returns
 */
export const injectRequestIdHeader = (headers?): object => {
  headers = headers || {};

  const requestId = getRequestId();
  if (requestId) {
    headers[headerName] = requestId;
  }

  return headers;
};

/**
 * Fastify auto define the header request-id into request object
 * @returns
 */
export const middlewareForFastify = (): Function => {
  return function (req, res, next) {
    // Start Context
    asyncStorage.run(req.id, () => {
      next();
    });
  };
};

/**
 * Capture header request-id
 * @param options
 * @returns
 */
export const middlewareForExpress = (options?): Function => {
  options = options || {};
  options.setHeader = options.setHeader === undefined || !!options.setHeader;
  options.headerName = options.headerName || headerName;

  return function (req, res, next) {
    // Define request-id
    const id = req.headers[options.headerName.toLowerCase()] || createRequestId();

    // Define a header on response
    if (options.setHeader) {
      res.setHeader(options.headerName, id);
    }

    // Start Context
    asyncStorage.run(id, () => {
      next();
    });
  };
};

export default {
  headerName,
  createRequestId,
  getRequestId,
  injectRequestIdHeader,
  middlewareForFastify,
  middlewareForExpress,
};

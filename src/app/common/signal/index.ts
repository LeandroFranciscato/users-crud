import { init as InitLogger } from "../logger";

import express from "../../../driver/http/express";

/**
 * Terminate application.
 */
const exit = () => {
  const logger = InitLogger();
  logger.log({
    message: "Exit application",
    level: "warn",
    tag: "exit",
  });
  process.exit(1);
};

/**
 * Stop all executions
 *
 * @example
 * ```typescript
 * process.on('SIGTERM', signal.shutdown);
 * process.on('SIGINT', signal.shutdown));
 * ```
 */
export const shutdown = () => {
  const logger = InitLogger();

  const { server } = express.initWithServer();

  logger.log({
    message: "Shutting down httpServer...",
    level: "warn",
    tag: "shutdown",
  });
  server.close(() => {
    exit();
  });
};

export default { shutdown };
